﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {


	public float multiplier = 0.5f;
	public float duration = 4f;
		
	public GameObject pickupEffect;
	Collider player;

	void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag ("Player")) {
			player = other;
			StartCoroutine (Pickup());
		}
		
	}

	IEnumerator Pickup()
	{
		Instantiate (pickupEffect, transform.position, transform.rotation);

		PlayerController rate = player.GetComponent<PlayerController> ();
		rate.fireRate /= multiplier; 

		GetComponent<MeshRenderer> ().enabled = false; 
		GetComponent<Collider> ().enabled = false;
		
		yield return new WaitForSeconds (duration);

		rate.fireRate *= multiplier;

		
		Destroy (gameObject);
	}
}
