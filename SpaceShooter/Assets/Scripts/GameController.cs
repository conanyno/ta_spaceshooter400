﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
	public GameObject hazard;
	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	public GameObject pill;
	public Vector3 spawnvalues;
	public int pillCount; 
	public float spawnDelay;
	public float startDelay;
	public float waveDelay;

	public GUIText scoreText;
	public GUIText restartText;
	public GUIText gameOverText;
	public GUIText HighScore;

	private bool gameOver;
	private bool restart;
	private int score;

	void Start ()
	{
		gameOver = false;
		restart = false;
		restartText.text = "";
		gameOverText.text = "";
		score = 0;
		HighScore.text = "High Score: " + PlayerPrefs.GetInt ("HighScore",0).ToString ();
		UpdateScore ();
		StartCoroutine (SpawnWaves ());
		StartCoroutine (SpawnPills ());
	}

	void Update ()
	{
		if (restart)
		{
			if (Input.GetKeyDown (KeyCode.R))
			{
				Application.LoadLevel (Application.loadedLevel);
			}
		}
	}

	IEnumerator SpawnPills ()
	{
		yield return new WaitForSeconds (startDelay);
		while (true)
		{
			for (int i = 0; i < pillCount; i++)
			{
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnvalues.x, spawnvalues.x), spawnvalues.y, spawnvalues.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (pill, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnDelay);
			}
			yield return new WaitForSeconds (waveDelay);

			if (gameOver)
			{
				restartText.text = "Press 'R' for Restart";
				restart = true;
				break;
			}
		}
	}

	IEnumerator SpawnWaves ()
	{
		yield return new WaitForSeconds (startWait);
		while (true)
		{
			for (int i = 0; i < hazardCount; i++)
			{
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds (waveWait);

			if (gameOver)
			{
				restartText.text = "Press 'R' for Restart";
				restart = true;
				break;
			}
		}
	}

	public void AddScore (int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore ();
	}

	void UpdateScore ()
	{
		scoreText.text = "Score: " + score;
		scoreText.text = score.ToString ();
			
		if (score > PlayerPrefs.GetInt ("HighScore")) {
			
			PlayerPrefs.SetInt ("HighScore", score);
			HighScore.text = score.ToString ();
		}
	
	}
	public void GameOver ()
	{
		print ("hello");
		gameOverText.text = "Game Over!";
		gameOver = true;
	}
}